<?php

namespace puffin;

$app->controller('img')
	->get('/img/view/{id:i}', 'view_by_dam_media_id')
	->get('/img/preview/{id:i}', 'preview_by_dam_media_id')
	->get('/img/tags/{tag}', 'view_by_tag');

<?php

namespace puffin;

$app->controller('media')
	->any('/media', 'index')
	->get('/media/create', 'create')
	->post('/media/create', 'do_create')
	->get('/media/update/{id:i}', 'update')
	->post('/media/update/{id:i}', 'do_update')
	->get('/media/delete/{id:i}', 'delete')
	->post('/media/delete/{id:i}', 'do_delete');

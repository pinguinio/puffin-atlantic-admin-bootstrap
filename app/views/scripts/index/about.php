<div class="card card-outline-secondary col-md-4">
	<h1 class="display-1">Hi there.</h1>
	<p class="lead">Here's the deal.</p>
	<p>At some point content jumped the damn fence and wandered off in new directions.</p>
	<p>CMSes are amazing tools. But they're just tools. They don't create ideas any more than a shovel, by itself, digs ditches.</p>
	<p>If you want an interesting website you have to actually BE interesting. No amount of WordPress plugins can make up the the lack of depth, of substance&mdash; OF SOUL.</p>
	<p>Content is King. The old institutions of its management are creaking in their dusty foundations. We aren't the revolution. We're the revolution that comes AFTER the revolution.</p>
	<p>Now, go create.</p>
</div>

<style>
#content{
	background-image: url('/theme/img/about.jpg');
	background-origin: border-box;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;

	font-weight: lighter;

	color:white;
}

</style>
